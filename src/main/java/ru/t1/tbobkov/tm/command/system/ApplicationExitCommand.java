package ru.t1.tbobkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    @NotNull
    public static final String DESCRIPTION = "close application";

    @NotNull
    public static final String NAME = "exit";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}