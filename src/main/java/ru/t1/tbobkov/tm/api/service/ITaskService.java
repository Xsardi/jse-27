package ru.t1.tbobkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.enumerated.Status;
import ru.t1.tbobkov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    Task updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @NotNull String description);

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @NotNull String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @Nullable Status status);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String id);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

}
