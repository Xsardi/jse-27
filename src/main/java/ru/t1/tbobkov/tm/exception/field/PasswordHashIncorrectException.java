package ru.t1.tbobkov.tm.exception.field;

public class PasswordHashIncorrectException extends AbstractFieldException {

    public PasswordHashIncorrectException() {
        super("Error! Password hash is incorrect...");
    }

}
